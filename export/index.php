<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/
    include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");   //Inclusion des variables globale
    requireClass("AffectationSemaine");
    requireClass("Module");
    requireClass("ContenuModule");
    requireClass("Module");
    requireClass('Enseignant');

    $moduleManager = new ModuleManager($connection);
    if(isset($_SESSION['id'])){
	    $enseignantManager = new EnseignantManager($connection);
	    $enseignantLogge = $enseignantManager->recup($_SESSION['id']);
    }
    $affectationSemaineManager = new AffectationSemaineManager($connection);
    $moduleManager = new ModuleManager($connection);
    $contenuModuleManager = new ContenuModuleManager($connection);
    
    $promo = $_GET['p'];
    
    $affectationsSemaine = $affectationSemaineManager->recupPlanningParPromotion($promo);
    $exportunit;
    $fileName;
    $contentType;
    
    
    if(isset($_GET['e'])){
        if($_GET['e'] == 'ical'){
            $fileName = "export-".time().".ical";
            $contentType="text/ical";
            
            $exportunit = "BEGIN:VCALENDAR\nVERSION:2.0\nPRODID:0\n";
            
            foreach($affectationsSemaine as $aff){            
                $module = $moduleManager->recupbyId($aff->getModule());
                $contenu = $contenuModuleManager->recupById($aff->getModule());
                $exportunit .= "BEGIN:VEVENT\n";
                $exportunit .= "DTSTART:";
                $exportunit .= "DTSTOP:";
                $exportunit .= "SUMMARY:".$aff->getModule()." ".$aff->getPartie()."\n";
                $exportunit .= "DESCRIPTION:Public : ".$module->getPublic()."\nEnseignant : ".$contenu->getEnseignant()."\nNb Heures : ".$aff->getNbHeures()."\n";
                $exportunit .= "END:VEVENT\n";
            }
            
            $exportunit .= "END:VCALENDAR\n";
        } else if($_GET['e'] == 'csv'){
            $fileName = "export-".time().".csv";
            $contentType="text/csv";
            
            $exportunit = "Module,Partie,Enseignant,Semaine,NbHeures\n";
            
            foreach($affectationsSemaine as $aff){
                $contenu = $contenuModuleManager->recupById($aff->getModule());

                $exportunit .= $aff->getModule().",";
                $exportunit .= $aff->getPartie().",";
                $exportunit .= $contenu->getEnseignant().",";
                $exportunit .= $aff->getSemaine().",";
                $exportunit .= $aff->getNbHeures()."\n";
            }
        }
        
        $result = file_put_contents("/tmp/".$fileName, $exportunit);
        if($result !== false){
            $file_url = '/tmp/'.$fileName;
            header('Content-Type: '.$contentType);
            header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
            readfile($file_url); // do the double-download-dance (dirty but worky)
        }
        exit;
    }
    
    require_once($viewRoot."exportView.php");
?>
