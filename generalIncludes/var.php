<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/

session_start();

$documentRoot = $_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/";


$serverRoot = ".";
for($i = 0 ; $i < (substr_count($_SERVER["REQUEST_URI"], "/") - 1) ; $i++){
    $serverRoot .= "/..";
}  

$serverRoot .= "/PlanningParSemaine/planningparsemaine/";

$coreRoot = $documentRoot."core/php/";
$viewRoot = $documentRoot."view/";

$enseignantsFile = $documentRoot."core/php/database/enseignants.csv";
$traceFile = $documentRoot."core/php/database/traces.csv";
$headFile = $documentRoot."generalIncludes/head.php";
$footFile = $documentRoot."generalIncludes/foot.php";

require_once($documentRoot."conf/connexion.php");

function requireClass($name){
    global $documentRoot;
    $name = ucfirst($name);
    require_once($documentRoot.'core/php/'.$name.".class.php");
    require_once($documentRoot.'core/php/manager/'.$name."Manager.class.php");
}



if(isset($_SESSION['id'])){
    requireClass("Enseignant");
    $enseignantManager = new EnseignantManager($connection);
    $enseignantLogge = $enseignantManager->recup($_SESSION['id']);
}

requireClass("Module");
$moduleManager = new ModuleManager($connection);
$promotionsTmp = $moduleManager->getAllPublic();
$menupromotions[] = "Sélectionnez une promotion";

foreach ($promotionsTmp as $prom){
    $menupromotions[] = $prom;
}
?>
