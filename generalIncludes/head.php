<?php
    /**
    *   Auteur : Jean-Baptiste Louvet, Antoine Nourry, Jean-Michel Nokaya, Matthieu Biache
    **/
?>
<!DOCTYPE html>

<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo $serverRoot; ?>images/favicon.ico">

    <title>Planning par semaines</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $serverRoot; ?>css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo $serverRoot; ?>css/dashboard.css" rel="stylesheet">
    <link href="<?php echo $serverRoot; ?>css/signin.css" rel="stylesheet">
    
    <style>
        div.course{
            height:10px;
            width:5px;
            background:red;
            margin:2px;
            display:inline-block;
        }
        .quantity{
            
        }
        div.week{
            height:50px;
            width:50px;
        }
        div.week.over {
            border: 2px dashed #000;
        }
    </style>

  </head>

  <body>
  
  <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo $serverRoot; ?>">PlanningParSemaine </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
          <!-- 	Si un enseignant est connecté, on fait apparâitre le lien "Profil" qui permet d'accéder sur la page d'édition de profil.
          	Si cet enseignant est un responsable d'année, alors on fait apparaître le lien "Editer planning" qui permet d'accéder à la page d'édition de planning-->
          <?php if(isset($_SESSION['id'])){
          		if($enseignantLogge->getAdministrateur() == 1){
          ?>
            <li><a href="<?php echo $serverRoot; ?>editPlanning/">Editer planning</a></li>
            <li><a href="<?php echo $serverRoot; ?>chargeEnseignant/">Charge des enseignants</a></li>
                <?php   } ?>
                <?php if($enseignantLogge->getAdministrateur() == 2){ ?>
                <li><a href="<?php echo $serverRoot; ?>chargeEnseignant/">Charge des enseignants</a></li>
                <li><a href="<?php echo $serverRoot; ?>dde/">Services réservés DDE</a></li>
                <?php } ?>
            <li><a href="<?php echo $serverRoot; ?>profile/">Profil de <?php echo $enseignantLogge->getPrenom().' '.$enseignantLogge->getNom(); ?></a></li>
            <li><a href="<?php echo $serverRoot; ?>deconnecter/">Déconnexion</a></li>
            <?php } else { ?>
            <li><a href="<?php echo $serverRoot; ?>">Connexion</a></li>
            <?php } ?>
            <?php if(isset($promotion)){?>
            <li><a href="<?php echo $serverRoot; ?>export/?p=<?php echo $promotion; ?>">Exporter</a></li>
            <?php } ?>
          </ul>
          <form class="navbar-form navbar-right" method="post" action="<?php echo $serverRoot; ?>planning/">
            <select name="choix">
            <?php
                foreach($menupromotions as $promoElem){ 
                    echo '<option value="'.$promoElem.'"';
                    
                    if(isset($promotion) && $promotion == $promoElem){
                        echo ' selected';
                    }
                    
                    echo '>'.$promoElem.'</option>';
                }
            ?>
            </select>
            
            <input type="submit" value="Consulter" name="Valider" />

          </form>
        </div>
      </div>
    </nav>
    
    <div class="container">
