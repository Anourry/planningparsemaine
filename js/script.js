﻿    /**
    *   Auteur : Jean-Baptiste Louvet, Matthieu Biache et Jean-Michel Nokaya
    **/
		//===== Gestion du glisser-deposer
		var dragSrcEl = null;

		//Pour la classe course
		function handleDragStart(e) {
			this.style.background = 'green';
			dragSrcEl = this;

			e.dataTransfer.effectAllowed = 'move';
			e.dataTransfer.setData('text/plain', this.id);
		}
		
		function handleDragEnd(e) {
			[].forEach.call(weeks, function (week) {
				week.classList.remove('over');
			});
		}
		
		//Pour la classe week
		function handleDragOver(e) {
			if (e.preventDefault) {
				e.preventDefault(); 
			}
				
			e.dataTransfer.dropEffect = 'move';

			return false;
		}

		
		function handleDragEnter(e) {
			this.classList.add('over');
		}

		function handleDragLeave(e) {
			this.classList.remove('over');
		}
		
		function handleDrop(e) {
			dragSrcEl.style.background = 'red';
		
			if(e.preventDefault){
				e.preventDefault();
			}
			
			if (e.stopPropagation) {		
				e.stopPropagation();
			}
			
			var sourceElemId = dragSrcEl.id.substring(0,dragSrcEl.id.length - 10);
			var targetElemId = this.id.substring(0,this.id.length-3);
			
			if(sourceElemId==targetElemId){
				var semOrig = dragSrcEl.id.substring(dragSrcEl.id.length-2,dragSrcEl.id.length);
				var semDest = this.id.substring(this.id.length-2,this.id.length);
				
				if(semOrig!=semDest) {
				
				var e1 = document.getElementById("s".concat(semOrig));
				var e2 = document.getElementById("s".concat(semDest));
				
				var c1 = e1.innerHTML;
				var c2 = e2.innerHTML;
				
				var act1=parseInt(c1.substring(c1.indexOf('">')+2,c1.indexOf('t>/')-1));
				var tot1=parseInt(c1.substring(c1.indexOf('t>/')+3,c1.indexOf('</b')));
				var act2=parseInt(c2.substring(c2.indexOf('">')+2,c2.indexOf('t>/')-1));
				var tot2=parseInt(c2.substring(c2.indexOf('t>/')+3,c2.indexOf('</b')));
					if(tot2>act2) {
					
					act1-=1;
					act2+=1;
					
					e1.innerHTML='<center><b><font color="'+(act1<0.75*tot1?'green':(act1<0.9*tot1?'orange':'red'))+'">'+act1+'</font>/'+tot1+'</b></center>';
					e2.innerHTML='<center><b><font color="'+(act2<0.75*tot2?'green':(act2<0.9*tot2?'orange':'red'))+'">'+act2+'</font>/'+tot2+'</b></center>';
					
					dragSrcEl.id = dragSrcEl.id.substring(0,dragSrcEl.id.length - 2)+semDest;
					
					var parentNode = dragSrcEl.parentNode;
					var ref = parentNode.removeChild(dragSrcEl);
					this.appendChild(ref);

					var moduleData = targetElemId.substring(0,targetElemId.indexOf('_'));
					var partieData = targetElemId.substring(targetElemId.indexOf('_')+1,targetElemId.length);
					var semaineOrigineData = semOrig;
					var semaineDestinationData = semDest;
                                        
					var xhr = new XMLHttpRequest();
					
					xhr.onreadystatechange = function(){
						if(xhr.readyState == 4 && (xhr.status== 200 || xhr.status == 0)){
							afficheSuccess();
						}
					} 
					
					xhr.open('POST', '../editPlanning/gestionServeurDragAndDrop.php');
					xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
					xhr.send('Module='+moduleData+'&Partie='+partieData+'&SemaineOrigine='+semaineOrigineData+'&SemaineDestination='+semaineDestinationData);
					
					} else {
						document.getElementById("planningInfo").outerHTML="<div id='planningInfo' class='alert alert-danger' role='alert'><strong>Action Impossible !</strong></div>";
					}
				}
				
			}
			
			
			return false;
		}
		
		function afficheSuccess(){
			document.getElementById("planningInfo").outerHTML="<div id='planningInfo' class='alert alert-success' role='alert'><strong>Planning modifié !</strong></div>";
		}
		
		var courses = document.querySelectorAll('.course');
		[].forEach.call(courses, function(course) {
			course.addEventListener('dragstart', handleDragStart, false);
			course.addEventListener('dragend', handleDragEnd, false);
		});
		
		var weeks = document.querySelectorAll('.week');
		[].forEach.call(weeks, function(week) {
			week.addEventListener('dragenter', handleDragEnter, false);
			week.addEventListener('dragover', handleDragOver, false);
			week.addEventListener('dragleave', handleDragLeave, false);
			week.addEventListener('drop', handleDrop, false);
		});   
