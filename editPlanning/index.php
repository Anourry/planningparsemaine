<?php
    /**
    *   Auteur : Jean-Baptiste Louvet, Matthieu Biache et Jean-Michel Nokaya
    **/
    /*function chargerClasse($classe){
        require_once $classe.'.class.php';
    }
    spl_autoload_register('chargerClasse');
    spl_autoload_register('afficherTableau');*/
    
    include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");	//Inclusion des variables globale
    requireClass("AffectationSemaine");		//chargement de la classe affectation semaine (metier et manager)
    requireClass("Module");
    requireClass('Enseignant');
    requireClass('Semaine');
if(isset($_SESSION['id'])){    
    $moduleManager = new ModuleManager($connection);
    
    $semaines = array();
    $semaineManager = new SemaineManager($connection);
    $semaines = $semaineManager->recup();
    
    $semaine = array();
    foreach($semaines as $value){
    	$semaine[$value->getSemaine()] = $value->getNombreHeuresMax();
    }
    
    /*if(isset($_SESSION['id'])){
    	var_dump($_SESSION['id']);
	    $enseignantManager = new EnseignantManager($connection);
	    $enseignantLogge = $enseignantManager->recup($_SESSION['id']);
    }*/
	
    //$promotions = $moduleManager->getAllPublic();
    
    //$promotions = array("TC","LSI1","LSI2","LSI3");
    
	$affectationSemaineManager = new affectationSemaineManager($connection);

	$promotion = trim($enseignantLogge->getPromotion());
	if(isset($_POST['loadTrace'])){
		//if(isset($_SESSION['affectationSemaine'])){
		
		$affectationSemaine = array();
		var_dump($affectationSemaineManager->loadLastTrace($traceFile));
		$recup = explode('|', $affectationSemaineManager->loadLastTrace($traceFile));
		
		foreach($recup as $value){
			$affectationSemaine[] = unserialize(base64_decode($value));
		}
			/*$tabTmp =array();
			
			$moduleTmp = $affectationSemaine[0]->getModule();

			// ***** création d'un tableau de modules (informations redondantes)
			foreach($affectationSemaine as $value){
				$partieTmp[] = $value->getModule();
			}
			
			// ***** création d'un tableau de modules uniques
			$partie = array();
			foreach($partieTmp as $value){
				if(!in_array($value, $partie)){
					$partie[] = $value;
				}
			}
			
			// ***** création de tableaux de nom égal à un élément du tableau précédent
			foreach($partie as $value){
				$$value = array();
				foreach($affectationSemaine as $val){
					if($val->getModule() == $value){
						array_push($$value, $val);
					}
				}
				$affectationSemaineManager->commitParModule($value, $$value);
			}*/
			

	} else {

        // ***** on rend stockable des objets PHP sous forme de chaînes de caractère
        $affectationSemaine = $affectationSemaineManager->recupPlanningParPromotion($promotion);
        
        /*$tabTemp = array();
        foreach($affectationSemaine as $value){
            $tabTemp[] = base64_encode(serialize($value));
        }
        // ***** on obtient un tableau de chaînes de caractère. On transforme ce tableau en chaîne de caractère pour l'envoyer en variable de session, en séparant chaque élément par |
        $stringAffectationSemaine = implode('|', $tabTemp);
        $_SESSION['affectationSemaine'] = $stringAffectationSemaine;*/
	
	}

	
	// ***** on a laissé l'obtention des données par la bdd pour s'assurer que la modification a bien été faite !
	
	
	
	
	/*if(!isset($_SESSION['affectationSemaine'])){
		// ***** on rend stockable des objets PHP sous forme de chaînes de caractère
        $tabTemp = array();
        foreach($affectationSemaine as $value){
            $tabTemp[] = base64_encode(serialize($value));
        }
        // ***** on obtient un tableau de chaînes de caractère. On transforme ce tableau en chaîne de caractère pour l'envoyer en variable de session, en séparant chaque élément par |
        $stringAffectationSemaine = implode('|', $tabTemp);
        $_SESSION['affectationSemaine'] = $stringAffectationSemaine;
	}*/
	
	if($affectationSemaine != null){

		$previousPartie = '';
		$previousModule = '';
			
			foreach($affectationSemaine as $affectation){
				if(!isset($nbParties[$affectation->getModule()])){
					$nbParties[$affectation->getModule()] = 0;
				}
			
				if($previousModule != $affectation->getModule()) {
					$previousPartie ='';
				}
			
				if(($previousPartie != $affectation->getPartie())) {
					$nbParties[$affectation->getModule()]++;
				}
				
				$previousModule = $affectation->getModule();
				$previousPartie = $affectation->getPartie();
			}
    
    }
}
    require_once($viewRoot."editPlanningView.php");
    
?>
