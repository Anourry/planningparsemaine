<?php
    /**
    *   Auteur : Jean-Baptiste Louvet, Matthieu Biache et Jean-Michel Nokaya
    **/

	include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");
	requireClass("AffectationSemaine");
	$affectationSemaineManager = new AffectationSemaineManager($connection);

	//recuperation
	if(isset($_POST['Module'])){
		$module = $_POST['Module'];
		$partie = $_POST['Partie'];
		$semaineOrig = intval($_POST['SemaineOrigine']);
		$semaineDest = intval($_POST['SemaineDestination']);
		
		$affectationSemaineOrig = $affectationSemaineManager->recupOne($module, $partie, $semaineOrig);
		$affectationSemaineOrig->setNbHeures($affectationSemaineOrig->getNbHeures() - 1);
		/*if(isset($_SESSION['affectationSemaine'])){
			$trouve = false;
			foreach($_SESSION['affectationSemaine'] as $value){
				if($value->getModule() == $affectationSemaineOrig->getModule()){
					$value = $affectationSemaineOrig;
					$trouve = true;
				}
			}
			if($trouve == false){
				
			} 
		}*/
		if($affectationSemaineOrig->getNbHeures() == 1) {
			$affectationSemaineManager->delete($affectationSemaineOrig);
		} else {
			$affectationSemaineOrig->setNbHeures($affectationSemaineOrig->getNbHeures() - 1);
		}
		
		$affectationSemaineManager->update($affectationSemaineOrig);
		
		$affectationSemaineDest = $affectationSemaineManager->recupOne($module, $partie, $semaineDest);
        if($affectationSemaineDest != NULL){
            $affectationSemaineDest->setNbHeures($affectationSemaineDest->getNbHeures() + 1);
            $affectationSemaineManager->update($affectationSemaineDest);
        } else {
            $affectationSemaineDest = new AffectationSemaine();
            $affectationSemaineDest->setModule($module);
            $affectationSemaineDest->setPartie($partie);
            $affectationSemaineDest->setSemaine($semaineDest);
            $affectationSemaineDest->setNbHeures(1);
            $affectationSemaineManager->insert($affectationSemaineDest);
        }
    }
	
	
	
?>
