<?php
    /**
    *   Auteur : Antoine Nourry
    **/
    include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");
    
    session_destroy();
    
    header("Location:".$serverRoot);
    
?>
