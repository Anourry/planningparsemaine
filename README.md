Projet php LSI2 -- ENSSAT

Affichage et interaction avec un planning de cour.

Installation et lancement du PlanningParSemaine.

Pre-requis:
	Avoir mysql et apache installé
	
Installation:
	Téléchargez les fichier. Décompressez le tout dans ~/apache2/htdocs
	
	Lancez le serveur apache (>application start apache)
	Lancez le serveur mysql (>application start mysql )

Dans mysql: (> mysql -u root) créez la base de donnée PLANNINGS (mysql> CREATE PLANNINGS) et remplissez la avec les fichier .sql

```
#!php

mysql> source [path]/1-dump\_schema.sql
mysql> source [path]/2-dump\_data.sql
mysql> source [path]/3-dump\_user.sql
mysql> use PLANNINGS
```


	

Utilisation:
Dans votre navigateur:
	Dans la barre de recherche tapez:
	http://localhost:8080/PlanningParSemaine/planningparsemaine/
	
Vous voici sur la page d'accueil.
	Vous pouvez vous connecter (login et mot de passe), ou directement consulter les emplois du temps des filiaires.
	Si vous vous connectez, vous pourrez:
		        - Modifier votre profil (Nom, Pseudo, Mot de passe,..
 		        - Consulter et (suivant vos droit)\* modifier le planning de certaines filiaires.
		        - Vous deconnecter

\* Les droits varient suivant les utilisateurs:
        - Les responsables de modules peuvent modifier leurs modules.
 	- Les responsable d'année peuvent modifier leurs années.
 	- La Direction Des Etudes (DDE) peut tout modifier, ainsi que consulter la charge de travail des enseignant. Il peuvent aussi nommer des responsables de module, ainsi que des enseignants chargés de module.
	
Pour modifier le planning, il suffit de glisser-déposer les crenneaux aux endroits souhaités. Attention neanmoins, le cota d'horaire/semaine est limité!	


Matthieu Biache // Nicolas Gunepin // Jean-Baptiste Louvet // Jean-Michel Nokaya // Antoine Nourry