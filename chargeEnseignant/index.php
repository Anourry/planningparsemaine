<?php
    /**
    * Auteur : Jean-Baptiste Louvet
    **/

    include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");   //Inclusion des variables globale
    requireClass("Enseignant");
    $enseignantManager = new EnseignantManager($connection);
    
    if(isset($_SESSION['id']) && ($enseignantManager->isAdmin($_SESSION['id']) == 'annee' || $enseignantManager->isAdmin($_SESSION['id']) == 'dde')){
        $enseignants = $enseignantManager->recupAll();
        $maxSemaine = 0;
        
        foreach($enseignants as $anEnseignant){
            $chargeEnseignants[0][] = $anEnseignant;
            $chargeTmp = $enseignantManager->getCharge($anEnseignant);
            $chargeEnseignants[1][] = $chargeTmp;
            for($i=0; $i < count($chargeTmp);$i++){
                if($chargeTmp[$i]['semaine'] > $maxSemaine){
                    $maxSemaine = $chargeTmp[$i]['semaine'];
                }
            }
        }
        
        require_once($viewRoot."chargeEnseignantView.php");
        exit;
    }
    
    header("Location: ".$serverRoot);    

?>