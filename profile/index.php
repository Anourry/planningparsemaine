<?php
    /**
    *   Auteur : Antoine Nourry
    **/
	 
	    include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php"); //inclusion des variables
	    requireClass("Module");
	    requireClass('Enseignant');
    
	    $moduleManager = new ModuleManager($connection);
	    $modifPseudo = false;
	    $pwdModif = false;
	    
            if( isset($_POST['confirm']) ){

	            // Mise a jour de l'objet enseignant
	            if( isset($_POST['pseudoNew']) ){                               //pseudo
	                $enseignantLogge->setLogin($_POST['pseudoNew']);
	                $modifPseudo = true;
	            }
	            if( isset($_POST['prenomNew']) ){                               //prenom
	                $enseignantLogge->setPrenom($_POST['prenomNew']);
	            }
	            if( isset($_POST['nomNew']) ){                                  //Nom
	                $enseignantLogge->setNom($_POST['nomNew']);
	            }
	            if( isset($_POST['newPwd']) && isset($_POST['newPwdVerif']) ) { //Mdp
	            	//echo 'BBIIIIIM JE SUIS RENTRE';
	                if($_POST['newPwd'] == $_POST['newPwdVerif'] && (strlen($_POST['newPwd']) > 4)){
	                    $enseignantLogge->setPwd($_POST['newPwd']);
	                    $pwdModif =true;
	                } else {
	                    $erreurModifPwd = true;		//permet d'afficher un message d'erreur mdp
	                }
	            }
	            	/*echo 'AVANT UPDATE';
	          	echo $enseignantLogge->getPwd();
			echo $enseignantLogge->getNom();
			echo $enseignantLogge->getPrenom();
			echo $enseignantLogge->getStatut();
			echo $enseignantLogge->getStatutaire();
			echo $enseignantLogge->getActif();
			echo $enseignantLogge->getAdministrateur();
			echo $enseignantLogge->getLogin();
*/			
		if($pwdModif){
			$enseignantManager->update($enseignantLogge);
			}else{
	            if($enseignantManager->updateSansPWD($enseignantLogge)){
	            	echo 'REUSSITE';
	            	}else{
	            	echo 'ECHEC';}
	            }	
	            	if($modifPseudo){       //Maj de la session si le pseudo a changé
	            		// ***** on s'assure que l'enseignant créé est bien celui que l'on vient d'entrer dans la base de données
	            		$enseignantLogge = $enseignantManager->recup($enseignantLogge->getLogin());
	                	$_SESSION['id'] = $enseignantLogge->getLogin();
	            	}
	            
	               //Mise a jour de la base
	            	/*echo '<br/><br/>APRES UPDATE';
	          	echo $enseignantLogge->getPwd();
			echo $enseignantLogge->getNom();
			echo $enseignantLogge->getPrenom();
			echo $enseignantLogge->getStatut();
			echo $enseignantLogge->getStatutaire();
			echo $enseignantLogge->getActif();
			echo $enseignantLogge->getAdministrateur();
			echo $enseignantLogge->getLogin();*/
            }
	    
	    require_once $viewRoot.'profileView.php';			//affichage de la vue
	    
	 
	    
?>
