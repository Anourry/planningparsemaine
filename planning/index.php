<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/
    /*function chargerClasse($classe){
        require_once $classe.'.class.php';
    }
    spl_autoload_register('chargerClasse');
    spl_autoload_register('afficherTableau');*/
    
    include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");	//Inclusion des variables globale
    requireClass("AffectationSemaine");		//chargement de la classe affectation semaine (metier et manager)
    requireClass("Module");
    requireClass('Enseignant');
    
    $moduleManager = new ModuleManager($connection);
    
    /*if(isset($_SESSION['id'])){
	    $enseignantManager = new EnseignantManager($connection);
	    $enseignantLogge = $enseignantManager->recup($_SESSION['id']);
    }*/
    //$promotions = $moduleManager->getAllPublic();
    
    //$promotions = array("TC","LSI1","LSI2","LSI3");
    
    if(isset($_POST['Valider']) && isset($_POST['choix'])){ 
        $affectationSemaineManager = new affectationSemaineManager($connection);
    
        $promotion = $_POST['choix'];
        $affectationSemaine = $affectationSemaineManager->recupPlanningParPromotion($promotion);
        
        if($affectationSemaine != null){
        
            $maxSemaine = $affectationSemaineManager->maxElemRequete($affectationSemaine, 'semaine');
            //$maxSemaine = 15; //recuperer le nombre de semaines max
            
            $partFound = array();			
            foreach($affectationSemaine as $affectation){
                if(!isset($nbParties[$affectation->getModule()])){
                    $nbParties[$affectation->getModule()] = 0;
                }
                
                if(!in_array($affectation->getModule().'.'.$affectation->getPartie(), $partFound)){
                    $partFound[] = $affectation->getModule().'.'.$affectation->getPartie();
                    $nbParties[$affectation->getModule()]++;
                }
            }
        }
    }
    
    require_once($viewRoot."planningView.php");
    
?>
