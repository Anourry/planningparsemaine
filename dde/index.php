<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/
    include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");   //Inclusion des variables globale
    requireClass("Enseignant");
    $enseignantManager = new EnseignantManager($connection);
    
    if(isset($_SESSION['id'])){
        $enseignantLog = $enseignantManager->recup($_SESSION['id']);
        if($enseignantLog->getAdministrateur() == 2){
            require_once($viewRoot."ddeIndexView.php");
            
            exit;
        }
    }
    
    header("Location: ".$serverRoot);
?>