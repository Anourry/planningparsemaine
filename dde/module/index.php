<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/
   
    include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");
    requireClass("Enseignant");
    $enseignantManager = new EnseignantManager($connection);
    
    if(isset($_SESSION['id'])){
        $enseignantLog = $enseignantManager->recup($_SESSION['id']);
        if($enseignantLog->getAdministrateur() == 2){
            requireClass("Module");
            requireClass("ContenuModule");
            
            $moduleManager = new ModuleManager($connection);
            $contenuModuleManager = new ContenuModuleManager($connection);
            
            if(isset($_POST['save'])){
                for($i = 0; $i < count($_POST['public']); $i++){
                    $data['public'] = $_POST['public'][$i];
                    $data['semestre'] = $_POST['semestre'][$i];
                    $data['module'] = $_POST['module'][$i];
                    $data['libelle'] = $_POST['libelle'][$i];
                    if(!empty($_POST['responsable'][$i])){
                        $data['responsable'] = $_POST['responsable'][$i];
                    } else {
                        $data['responsable'] = NULL;
                    }
                    
                    $module = new Module ($data);
                    $moduleManager->update($module);
                }
      
                for($i = 0; $i < count($_POST['partie']); $i++){
                    $data['module'] = $_POST['moduleContenu'][$i];
                    $data['partie'] = $_POST['partie'][$i];
                    $data['type'] = $_POST['type'][$i];
                    $data['nbHeures'] = $_POST['nbHeures'][$i];
                    if(!empty($_POST['enseignant'][$i])){
                        $data['enseignant'] = $_POST['enseignant'][$i];
                    } else {
                        $data['enseignant'] = NULL;
                    }
                    
                    $contenuModule = new ContenuModule($data);
                    $contenuModuleManager->update($contenuModule);
                }                    
            }
            
            $modules = $moduleManager->getAllModules();
              
            foreach($modules[0] as $aModule){
                if(!isset($nbParties[$aModule->getModule()])){
                    $nbParties[$aModule->getModule()] = 0;
                }
                
                $nbParties[$aModule->getModule()]++;
            }
            
            $enseignantManager = new EnseignantManager($connection);
            $enseignantsDisponibles = $enseignantManager->recupAll();
            
            if(isset($_POST['edit'])){
                $edit = true;
            }
            
            require_once($viewRoot."moduleView.php"); 
            
            exit;
        }
    }
    
    header("Location: ".$serverRoot);    
?>