<?php
/**
*   Auteur : Jean-Baptiste Louvet
**/

    $serverRoot = ".";
    for($i = 0 ; $i < (substr_count($_SERVER["REQUEST_URI"], "/") - 1) ; $i++){
        $serverRoot .= "/..";
    }  

    $serverRoot .= "/PlanningParSemaine/planningparsemaine/";

    header("Location:".$serverRoot);
?>