<?php
    /**
    *   Auteur : Jean-Baptiste Louvet et Antoine Nourry
    **/
?>

<?php
    include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");
    requireClass('enseignant');
    requireClass("Module");
    requireClass('Enseignant');
    
    $moduleManager = new ModuleManager($connection);
	
	$enseignantManager = new EnseignantManager($connection);     //création de l'objet qui verifira les param de login. 
	
	if (isset($_POST['pseudo']) && isset($_POST['pwd']) ){
		$pwd = $_POST['pwd'];
		$pseudo = $_POST['pseudo'];
		  	
        $mauvaisPseudo = !($enseignantManager->recup($pseudo,$pwd)); //retourne True si le pseudo correspond o mdp
				  	
		if( !$mauvaisPseudo ){ 
			$_SESSION['id']=$pseudo;        //Si login correct, creation d'une session correspondante
	    }	
	}
	
	if(isset($_SESSION['id'])){
        $enseignant = $enseignantManager->recup($_SESSION['id']);
        $enseignantLogge = $enseignantManager->recup($_SESSION['id']);
	}

//    require_once("view/indexView.php");
    
    requireClass("AffectationSemaine");     //chargement de la classe affectation semaine (metier et manager)
        
    if(isset($_POST['Valider']) && isset($_POST['choix'])){ 
        $affectationSemaineManager = new affectationSemaineManager($connection);
    
        $promotion = $_POST['choix'];
        $affectationSemaine = $affectationSemaineManager->recupPlanningParPromotion($promotion);
        
        if($affectationSemaine != null){
        
            $maxSemaine = $affectationSemaineManager->maxElemRequete($affectationSemaine, 'semaine');
            
            foreach($affectationSemaine as $affectation){
                if(!isset($nbParties[$affectation->getModule()])){
                    $nbParties[$affectation->getModule()] = 0;
                }
                
                $nbParties[$affectation->getModule()]++;
            }
        }
    }
    
   require_once($viewRoot."accueilView.php");
?>
