﻿<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/
?>

<?php include($headFile); ?>     
        <?php if(isset($promotion) && isset($affectationSemaine)){ ?>
		<h3>Affichage du planning par promotion</h3>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr><th>Module</th><th>Nature</th>
                        <?php for($i = 1; $i <= $maxSemaine; $i++){
                            echo '<th>'.$i.'</th>';
                        } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $prevMod = "init";
                        $prevType = "";
                        $currentSem = 1;
                        
                        foreach($affectationSemaine as $affectation){
                            if($prevMod == 'init'){
                                echo '<tr><td rowspan="'.$nbParties[$affectation->getModule()].'">'.$affectation->getModule().'</td><td>'.$affectation->getPartie().'</td>';
                                $prevMod = $affectation->getModule();
                                $prevType = $affectation->getPartie();
                                $currentSem = 1;
                            } else if($prevMod != $affectation->getModule()){
                                for($currentSem; $currentSem <= $maxSemaine; $currentSem++){
                                    echo '<td class="week"></td>';
                                }
                                echo '</tr><tr><td rowspan="'.$nbParties[$affectation->getModule()].'">'.$affectation->getModule().'</td><td>'.$affectation->getPartie().'</td>';
                                $prevMod = $affectation->getModule();
                                $prevType = $affectation->getPartie();
                                $currentSem = 1;
                            } else if($prevType != $affectation->getPartie()){
                                for($currentSem; $currentSem <= $maxSemaine; $currentSem++){
                                    echo '<td class="week"></td>';
                                }
                                echo '</tr><tr><td>'.$affectation->getPartie().'</td>';
                                $prevType = $affectation->getPartie();
                                $currentSem = 1;
                            }
                            
                            for($currentSem; $currentSem < $affectation->getSemaine(); $currentSem++){
                                echo '<td class="week"></td>';
                            }
                            
                            echo '<td class="week">';
                            for($i = 0; $i < $affectation->getNbHeures(); $i++){
                                echo '<div class="course" id="'.$affectation->getModule().'_'.$affectation->getPartie().'_item'.$i.'" draggable="true"></div>';
                            }
                            echo ' ('.$affectation->getNbHeures().'h)';
                            echo '</td>';
                            $currentSem++;
                        }
                        
                        for($currentSem; $currentSem <= $maxSemaine; $currentSem++){
                            echo '<td class="week"></td>';
                        } ?>
                        </tr>
                    </tbody>
                </table>
            </div>
		
		<?php } else { ?>       
        <div class="alert alert-danger" role="alert">
          <strong>Planning Vide !</strong> Veuillez sélectionner une promotion valide.
        </div>
		
		<center><img src="../img/image2.jpg" /><center>
        <?php } ?> 
        


<?php include($footFile); ?>
