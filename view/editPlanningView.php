﻿<?php
    /**
    *   Auteur : Jean-Michel Nokaya et Matthieu Biache
    **/
?>

<?php include($headFile); ?>
		<?php if(isset($_SESSION['id'])){ ?>
		<h3>Affichage et édition du planning par promotion : <?php echo $promotion; ?> </h3>

        <?php if(isset($promotion) && isset($affectationSemaine)){ ?>
            <div class="table-responsive">
                <table border="1" class="table table-striped">
                    <thead>
                        <tr><th>Module</th><th>Nature</th>
						
                        <?php
						$heures = array();
						$maxSemaine=0;
						foreach ($semaine as $numeroSemaine => $heuresMax){
							$heures[$numeroSemaine]=0;
							echo '<th><center>'.$numeroSemaine.'</center></th>';
							$maxSemaine=$numeroSemaine;
						}
						 ?>
						
                        </tr>
                    </thead>
                    <tbody>
						<?php
						
						$minSemaine = 3;
						$prevMod = "init";
                        $prevType = "";
                        $currentSem = $minSemaine;
                        
                        foreach($affectationSemaine as $affectation){
                            if($prevMod == 'init'){
                                echo '<tr><td rowspan="'.$nbParties[$affectation->getModule()].'">'.$affectation->getModule().'</td><td>'.$affectation->getPartie().'</td>';
                                $prevMod = $affectation->getModule();
                                $prevType = $affectation->getPartie();
                                $currentSem = $minSemaine;
                            } else if($prevMod != $affectation->getModule()){
                                for($currentSem; $currentSem <= $maxSemaine; $currentSem++){
                                    echo '<td id="'.$prevMod.'_'.$prevType.'_'.($currentSem<10?"0".$currentSem:$currentSem).'" class="week"></td>';
                                }
                                echo '</tr><tr><td rowspan="'.$nbParties[$affectation->getModule()].'">'.$affectation->getModule().'</td><td>'.$affectation->getPartie().'</td>';
                                $prevMod = $affectation->getModule();
                                $prevType = $affectation->getPartie();
                                $currentSem = $minSemaine;
                            } else if($prevType != $affectation->getPartie()){
                                for($currentSem; $currentSem <= $maxSemaine; $currentSem++){
                                    echo '<td id="'.$prevMod.'_'.$prevType.'_'.($currentSem<10?"0".$currentSem:$currentSem).'" class="week"></td>';
                                }
                                echo '</tr><tr><td>'.$affectation->getPartie().'</td>';
                                $prevType = $affectation->getPartie();
                                $currentSem = $minSemaine;
                            }
                            
                            for($currentSem; $currentSem < $affectation->getSemaine(); $currentSem++){
                                echo '<td id="'.$affectation->getModule().'_'.$affectation->getPartie().'_'.($currentSem<10?"0".$currentSem:$currentSem).'" class="week"></td>';
                            }
                            
                            echo '<td id="'.$affectation->getModule().'_'.$affectation->getPartie().'_'.($currentSem<10?"0".$currentSem:$currentSem).'" class="week">';
                            $heures[$currentSem]+=$affectation->getNbHeures();
							for($i = 0; $i < $affectation->getNbHeures(); $i++){
                                echo '<div class="course" id="'.$affectation->getModule().'_'.$affectation->getPartie().'_item'.($i<10?"0".$i:$i).'_'.($currentSem<10?"0".$currentSem:$currentSem).'" draggable="true"></div>';
                            }
                            echo '</td>';
                            $currentSem++;
                        }
                        
                        for($currentSem; $currentSem <= $maxSemaine; $currentSem++){
                            echo '<td id="'.$affectation->getModule().'_'.$affectation->getPartie().'_'.($currentSem<10?"0".$currentSem:$currentSem).'" class="week"></td>';
                        } ?>
                        </tr>
						<tr><th colspan="2">Heures max</th>
                        <?php 
						
						                        
						foreach ($semaine as $numeroSemaine => $heuresMax){
							$act=$heures[$numeroSemaine];
							$tot=$heuresMax;
						
							echo '<td id="s'.($numeroSemaine<10?"0".$numeroSemaine:$numeroSemaine).'"><center><b><font color="'.($act<0.75*$tot?'green':($act<0.9*$tot?'orange':'red')).'">'.$act.'</font>/'.$tot.'</b></center></td>';
						}
						
						?>
						</tr>
                    </tbody>
                </table>
            </div>
	<?php } ?>
	
	<div id="planningInfo"></div>
	<!-- Etape 8 non fonctionnelle
	<form action="<?php echo $serverRoot?>editPlanning/index.php" method="POST">
        	<input type="submit" name="loadTrace" value = "Annuler les modifications" />
    	</form>
    	-->
    <?php } else { ?>       
        <div class="alert alert-danger" role="alert">
          <strong>Planning Vide !</strong> Responsable d'année : veuillez vous connecter. Vous n'êtes pas responsable d'année ? Vous ne pouvez pas éditer de planning !
        </div>
		
		<center><img src="../img/image2.jpg" /><center>
        <?php } ?> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
	<script src="../js/script.js"></script>
	
<?php include($footFile); ?>

