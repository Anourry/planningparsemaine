<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/
?>

<?php include($headFile); ?>

<form class="form" action="./" method="GET">
    <h2 class="form-signin-heading">Choisissez le format d'exportation du planning <?php echo $promo; ?></h2>
    <input type="hidden" name="p" value="<?php echo $promo; ?>"/>
    <input type="radio" name="e" value="csv" id="radioCSV" /><label for="radioCSV">Format CSV</label>
    <input type="radio" name="e" value="ical" id="radioICAL" /><label for="radioICAL">Format iCal</label>
    <br/>
    <input type="submit" value="Exporter" class="btn btn-lg btn-primary"/>
</form>

<?php include($footFile); ?>