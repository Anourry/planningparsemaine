<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/
?>

<?php include($headFile); ?>

<h3>Affichage des charges de service enseignant</h3>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Nom</th><th>Prénom</th>
                <th>Nombre d'heures de CM</th>
                <th>Nombre d'heures de TD</th>
                <th>Nombre d'heures de TP</th>
                <th>Nombre d'heures de Projet</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($serviceEnseignant as $aService){ 
            if($aService != NULL){ $totalHeure = 0; ?>
            <tr>
                <td><?php echo $aService['nom']; ?></td>
                <td><?php echo $aService['prenom']; ?></td>
                <td><?php echo $aService['CM']; $totalHeure += $aService['CM']; ?></td>
                <td><?php echo $aService['TD']; $totalHeure += $aService['TD']; ?></td>
                <td><?php echo $aService['TP']; $totalHeure += $aService['TP']; ?></td>
                <td><?php echo $aService['Projet']; $totalHeure += $aService['Projet']; ?></td>
                <td><?php echo $totalHeure; ?></td>
            </tr>
        <?php } } ?>
        </tbody>
    </table>
</div>

<?php include($footFile); ?>