<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/
?>

<?php include($headFile); ?>

<div class="jumbotron">
    <h1>Services réservés à la direction des études</h1>
    
    <ul>
        <li><a href="<?php echo $serverRoot; ?>dde/module/">Edition de modules</a></li>
        <li><a href="<?php echo $serverRoot; ?>dde/service/">Charges de service enseignant</a></li>
    </ul>
</div>
<?php include($footFile); ?>