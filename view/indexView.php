<?php
    /**
    *   Auteur : Jean-Baptiste Louvet et Antoine Nourry
    **/
?>

<?php include($headFile); ?>
     
     <?php if( !(isset($_SESSION['id'])) ){ ?>
      <form class="form-signin" action="<?php echo $_SERVER['REQUEST_URI']?>" method="POST">
        <h2 class="form-signin-heading">Connectez-vous.</h2>
        <?php if(isset($mauvaisPseudo) && $mauvaisPseudo){ ?>       <!-- N'est afficher que si il y a une erreur de pseudo/mdp-->
        <div class="alert alert-danger" role="alert">
          <strong>Erreur .</strong> Mauvais nom d'utilisateur et/ou mot de passe.
        </div>
        <?php } ?>    
        
        <label for="inputEmail" class="sr-only">Identifiant</label>
        <input type="text" id="inputEmail" class="form-control" placeholder="Identifiant" name="pseudo" <?php if(isset($mauvaisPseudo) && $mauvaisPseudo){ echo 'value="'.$pseudo.'"';  } ?> required autofocus>
        <label for="inputPassword" class="sr-only">Mot de passe</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" name="pwd" required>
       <!-- <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div>
            -->
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
      
      <?php } else { ?>         <!--Login Success -->
      <!-- <h2 class="form-signin-heading">Connectez vous STP</h2>  //Solution n°2 pour affichage propre--> 
      <div class="alert alert-success" role="alert">
        <strong>Félicitations !</strong> Bienvenue <?php echo $enseignant->getPrenom()." ".$enseignant->getNom();  ?>.
      </div>
	  		
      <?php } ?>
      


<?php include($footFile); ?>
