<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/
?>

<?php include($headFile); ?>
<div class="table-responsive">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Enseignant</th>
                <?php for($i=1; $i <= $maxSemaine ; $i++){ ?>
                <th><?php echo $i; ?></th>
                <?php } ?>
           </tr>
        </thead>
        <tbody>
            <?php for($i=0; $i < count($chargeEnseignants[0]); $i++){ ?>
            <?php $curSem = 1; ?>
            <tr>
                <td><?php echo $chargeEnseignants[0][$i]->getPrenom().' '.$chargeEnseignants[0][$i]->getNom(); ?></td>
                <?php if($chargeEnseignants[1][$i][0] !== FALSE){ ?>
                    <?php for($j = 0; $j < count($chargeEnseignants[1][$i]); $j++){
                        for($curSem; $curSem < $chargeEnseignants[1][$i][$j]['semaine']; $curSem++){
                            echo "<td></td>";
                        }
                        
                        if($chargeEnseignants[1][$i][$j] !== FALSE){
                            echo "<td>".$chargeEnseignants[1][$i][$j]['charge']."h</td>";
                            $curSem++;
                        }
                    } ?>
                <?php } ?>
                <?php for($curSem; $curSem <= $maxSemaine; $curSem++){
                        echo "<td></td>";
                } ?>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<?php include($footFile); ?>