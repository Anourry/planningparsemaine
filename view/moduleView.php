<?php
    /**
    *   Auteur : Jean-Baptiste Louvet
    **/
?>

<?php include($headFile); ?>
<?php $prevMod = ""; ?>

<form action="." method="POST">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Public</th>
                    <th>Semestre</th>
                    <th>Module</th>
                    <th>Libelle</th>
                    <th>Responsable</th>
                    <th>Partie</th>
                    <th>Type</th>
                    <th>Nb Heures</th>
                    <th>Enseignant</th>
                </tr>
            </thead>
            
            <?php if(isset($edit) && $edit) { ?>
            <tbody>
                <?php for($i = 0; $i < count($modules[0]); $i++){ ?>
                <tr>
                    <?php if($modules[0][$i]->getModule() != $prevMod){ ?>
                    <td rowspan='<?php echo $nbParties[$modules[0][$i]->getModule()]; ?>'>
                        <input type="text" name="public[]" value="<?php echo $modules[0][$i]->getPublic(); ?>" />
                    </td><td rowspan='<?php echo $nbParties[$modules[0][$i]->getModule()]; ?>'>
                        <select name="semestre[]">
                            <option value="S1" <?php if($modules[0][$i]->getSemestre() == "S1"){ echo "selected"; } ?>>S1</option>
                            <option value="S2" <?php if($modules[0][$i]->getSemestre() == "S2"){ echo "selected"; } ?>>S2</option>
                        </select>
                    </td><td rowspan='<?php echo $nbParties[$modules[0][$i]->getModule()]; ?>'>
                        <?php echo $modules[0][$i]->getModule(); ?>
                        <input type="hidden" name="module[]" value="<?php echo $modules[0][$i]->getModule(); ?>" />
                    </td><td rowspan='<?php echo $nbParties[$modules[0][$i]->getModule()]; ?>'>
                        <input type="text" name="libelle[]" value="<?php echo $modules[0][$i]->getLibelle(); ?>" />
                    </td><td rowspan='<?php echo $nbParties[$modules[0][$i]->getModule()]; ?>'>
                        <select name="responsable[]">
                        <option value=""></option>
                        <?php foreach($enseignantsDisponibles as $enseignant){ ?>
                            <option 
                                <?php if($modules[0][$i]->getResponsable() == $enseignant->getLogin()){ 
                                    echo "selected"; 
                                }?>
                                value="<?php echo $enseignant->getLogin(); ?>">
                                <?php echo $enseignant->getPrenom().' '.$enseignant->getNom(); ?>
                            </option>
                        <?php } ?>
                        </select>
                    </td><?php $prevMod = $modules[0][$i]->getModule(); } ?><td>
                        <?php echo $modules[1][$i]->getPartie(); ?>
                        <input type="hidden" name="partie[]" value="<?php echo $modules[1][$i]->getPartie(); ?>" />
                        <input type="hidden" name="moduleContenu[]" value="<?php echo $modules[1][$i]->getModule(); ?>" />
                    </td><td>
                        <select name="type[]">
                            <option value="CM" <?php if($modules[1][$i]->get_Type() == "CM"){ echo "selected"; }?>>CM</option>
                            <option value="TD" <?php if($modules[1][$i]->get_Type() == "TD"){ echo "selected"; }?>>TD</option>
                            <option value="TP" <?php if($modules[1][$i]->get_Type() == "TP"){ echo "selected"; }?>>TP</option>
                            <option value="Projet" <?php if($modules[1][$i]->get_Type() == "Projet"){ echo "selected"; }?>>Projet</option>
                        </select>
                    </td><td>
                        <input type="number" name="nbHeures[]" value="<?php echo $modules[1][$i]->getNbHeures(); ?>" />
                    </td><td>                    
                        <select name="enseignant[]">
                        <option value=""></option>
                        <?php foreach($enseignantsDisponibles as $enseignant){ ?>
                            <option 
                                <?php if($modules[1][$i]->getEnseignant() == $enseignant->getLogin()){ 
                                    echo "selected"; 
                                }?>
                                value="<?php echo $enseignant->getLogin(); ?>">
                                <?php echo $enseignant->getPrenom().' '.$enseignant->getNom(); ?>
                            </option>
                        <?php } ?>
                        </select>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            <?php } else { ?>
            <tbody>
                <?php for($i = 0; $i < count($modules[0]); $i++){ ?>
                <tr>
                    <?php if($modules[0][$i]->getModule() != $prevMod){ ?>
                    <td rowspan='<?php echo $nbParties[$modules[0][$i]->getModule()]; ?>'>
                    <?php echo $modules[0][$i]->getPublic(); ?>
                    </td><td rowspan='<?php echo $nbParties[$modules[0][$i]->getModule()]; ?>'>
                    <?php echo $modules[0][$i]->getSemestre(); ?>
                    </td><td rowspan='<?php echo $nbParties[$modules[0][$i]->getModule()]; ?>'>
                    <?php echo $modules[0][$i]->getModule(); ?>
                    </td><td rowspan='<?php echo $nbParties[$modules[0][$i]->getModule()]; ?>'>
                    <?php echo $modules[0][$i]->getLibelle(); ?>
                    </td><td rowspan='<?php echo $nbParties[$modules[0][$i]->getModule()]; ?>'>
                    <?php echo $modules[0][$i]->getResponsable(); ?>
                    </td><?php $prevMod = $modules[0][$i]->getModule(); } ?><td>
                    <?php echo $modules[1][$i]->getPartie(); ?>
                    </td><td>
                    <?php echo $modules[1][$i]->get_Type(); ?>
                    </td><td>
                    <?php echo $modules[1][$i]->getNbHeures(); ?>
                    </td><td>
                    <?php echo $modules[1][$i]->getEnseignant(); ?>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
            <?php } ?>
        </table>
    </div>

    <?php if(isset($edit) && $edit) { ?>
        <input type="submit" class="btn btn-primary" name="save" value="Enregistrer"/>
    <?php } else { ?>
        <input type="submit" class="btn btn-primary" name="edit" value="Modifier"/>
    <?php } ?>
    
</form>
<?php include($footFile); ?>