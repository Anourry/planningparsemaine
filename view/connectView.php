<?php
    /**
    *   Auteur : Antoine Nourry
    **/
?>

    <?php if( !(isset($_SESSION['id'])) ){ ?>
      <form class="form-signin" action="<?php echo $_SERVER['REQUEST_URI']?>" method="POST">
        <h2 class="form-signin-heading">Connectez-vous:</h2>
       
        <?php if(isset($mauvaisPseudo) && $mauvaisPseudo){ ?>       <!-- N'est affiche que si il y a une erreur de pseudo/mdp-->
        <div class="alert alert-danger" role="alert">
          <strong>Fichtre!</strong> Mauvais nom d'utilisateur et/ou mot de passe
        </div>
        <?php } ?>    
       
        <label for="inputEmail" class="sr-only">Identifiant:</label>
        <input type="text" id="inputEmail" class="form-control" placeholder="Identifiant" name="pseudo" <?php if(isset($mauvaisPseudo) && $mauvaisPseudo){ echo 'value="'.$pseudo.'"';  } ?> required autofocus>
        <label for="inputPassword" class="sr-only">Mot de passe:</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Mot de passe" name="pwd" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>
      <!--Si deja connecte-->
      <?php } else if(!(isset($_SESSION['bienvenue'])) ){ ?>
      <div class="alert alert-success" role="alert">
        <strong>Félicitations ! Vous êtes connecté !</strong><br> Bienvenue <?php echo $enseignantLogge->getPrenom()." ".$enseignantLogge->getNom();  ?> !
	</div>
      
      <?php $_SESSION['bienvenue']='';} ?>
