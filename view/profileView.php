
<?php
    /**
    *   Auteur : Antoine Nourry
    **/
?>

<?php include($headFile); ?>
     
     <?php if( (isset($_SESSION['id'])) ){ ?>
     
     <!-- Partie modifiable -->
      <form class="form-signin" action="<?php echo $_SERVER['REQUEST_URI']?>" method="POST">
        <h2>Profil:</h2>
            <h3>Prénom:</h3>
        <label for="inputEmail" class="sr-only">Prénom</label>
        <input type="text" id="inputEmail" class="form-control" placeholder="prenom" name="prenomNew" value="<?php echo $enseignantLogge->getPrenom(); ?>" >
            <h3>Nom:</h3>
        <label for="inputEmail" class="sr-only">Nom</label>
        <input type="text" id="inputEmail" class="form-control" placeholder="nom" name="nomNew" value="<?php echo $enseignantLogge->getNom(); ?>" >
        
            <h3>Mot de passe:</h3>
         <?php if(isset($erreurModifPwd) && $erreurModifPwd){ ?>       <!-- N'est afficher que si il y a une erreur de pseudo/mdp-->
        <div class="alert alert-danger" role="alert">
          <strong>Erreur .</strong> Retapez le nouveau mot de passe svp. (De longueur > 4 characteres).
        </div>
        <?php } ?>    
        
        <label for="inputPassword" class="sr-only">Mot de passe</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Nouveau Mot de passe" name="newPwd"  value="<?php echo $enseignantLogge->getPwd();?>"/>
        <input type="password" id="inputPassword" class="form-control" placeholder="Verification du nouveau Mot de passe" name="newPwdVerif" value="<?php echo $enseignantLogge->getPwd();?>"/>
        
        <button class="btn btn-lg btn-primary btn-block" name="confirm" type="submit">Modifier</button>
      </form>
      
      
      
      <!-- Partie information: statut, statutaire, actif , administrateur -->
      <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">statutaire</h3>
            </div>
            <div class="panel-body">
              <?php echo $enseignantLogge->getStatutaire(); ?>
            </div>
          </div>
      </div>
      <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">actif</h3>
            </div>
            <div class="panel-body">
              <?php echo $enseignantLogge->getActif(); ?>
            </div>
          </div>
      </div>
      <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">statut</h3>
            </div>
            <div class="panel-body">
              <?php echo $enseignantLogge->getStatut(); ?>
            </div>
          </div>
      </div>
      <div class="col-sm-4">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">administrateur</h3>
            </div>
            <div class="panel-body">
              <?php echo $enseignantLogge->getAdministrateur(); ?>
            </div>
          </div>
     </div>
        
      
      <?php } else { 
            header("Location:".$serverRoot);
            } ?>
      


<?php include($footFile); ?>
