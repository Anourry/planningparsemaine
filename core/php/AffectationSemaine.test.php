<?php
	/*function chargerClasse($classe){
		require_once $classe.'.class.php';
	}
	spl_autoload_register('chargerClasse');
	spl_autoload_register('afficherTableau');*/
	
	require_once 'manager/AffectationSemaineManager.class.php';
    require_once 'AffectationSemaine.class.php';
    
	require_once("../../conf/connexion.php");
	$promotions = array("TC","LSI1","LSI2","LSI3");
	
	function afficherTableau($connection){
				$tableau =[];
				if(isset($_POST['Valider'])){ 
					$tableau[] = '<table border=1 >';
					if(isset($_POST['choix'])){
						$affectationSemaineManager = new AffectationSemaineManager($connection);
						$promotion = $_POST['choix'];
					
						$tableau[] = '<tr><th colspan="2">Module\Semaine</th>';
					
						$moduleParSemaine = $affectationSemaineManager->recupPlanningParPromotion($_POST['choix']);
						$maxSemaine = $affectationSemaineManager->maxElemRequete($moduleParSemaine, 'semaine');
						
						$header = "";
						for($i = 1; $i < $maxSemaine + 1; $i++){
							$header = $header .'<th>'.$i.'</th>';
						}
						
						$tableau[] = $header;
						
					
						$doublonLigne ="";
						$ligneTableau = "";
						$compteur1 = 1;
						$compteurElse = 0;
						$doublonSemaine = 0;
						foreach($moduleParSemaine as $ligne){
						
							if($ligne['module'] != $doublonLigne){
								if($compteur1 >= 1){
									$tableau[] = $ligneTableau;
								}
								$compteur1 = 0;
								
								$ligneTableau = '<tr>';
								$ligneTableau = $ligneTableau.'<td rowspan ="4">'.$ligne['module'].'</td>';
								$ligneTableau = $ligneTableau.'</tr>';

								//Si la ligne de requếte étudiée est un CM :
								if(strpos($ligne['partie'], "CM") !== false){
									//ligne CM
									$ligneTableau = $ligneTableau.'<tr><td>CM</td>';
									for($i = 1; $i < $maxSemaine + 1; $i++){
										if($i == $ligne['semaine']){
											$ligneTableau = $ligneTableau.'<td>'.$ligne['nbHeures'].'h</td>';
										}
										$ligneTableau = $ligneTableau.'<td> </td>';
									}
									$ligneTableau = $ligneTableau.'</tr>';
									//ligne TD
									$ligneTableau = $ligneTableau.'<tr><td>TD</td>';
									for($i = 1; $i < $maxSemaine + 1; $i++){
										$ligneTableau = $ligneTableau.'<td> </td>';
									}
									$ligneTableau = $ligneTableau.'</tr>';
									//ligne TP
									$ligneTableau = $ligneTableau.'<tr><td>TP</td>';
									for($i = 1; $i < $maxSemaine + 1; $i++){
										$ligneTableau = $ligneTableau.'<td> </td>';
									}
									$ligneTableau = $ligneTableau.'</tr>';
								
								//Si la ligne de requête étudiée est un TD :
								}elseif(strpos($ligne['partie'], "TD") !== false){
									//ligne CM
									$ligneTableau = $ligneTableau.'<tr><td>CM</td>';
									for($i = 1; $i < $maxSemaine + 1; $i++){
										$ligneTableau = $ligneTableau.'<td> </td>';
									}
									$ligneTableau = $ligneTableau.'</tr>';
									//ligne TD
									$ligneTableau = $ligneTableau.'<tr><td>TD</td>';
									for($i = 1; $i < $maxSemaine + 1; $i++){
										if($i == $ligne['semaine']){
											$ligneTableau = $ligneTableau.'<td>'.$ligne['nbHeures'].'h</td>';
										}
										$ligneTableau = $ligneTableau.'<td> </td>';
									}
									$ligneTableau = $ligneTableau.'</tr>';
									//ligne TP
									$ligneTableau = $ligneTableau.'<tr><td>TP</td>';
									for($i = 1; $i < $maxSemaine + 1; $i++){
										$ligneTableau = $ligneTableau.'<td> </td>';
									}
									$ligneTableau = $ligneTableau.'</tr>';
							
								//Si la ligne de requête étudiée est un TP :
								}elseif(strpos($ligne['partie'], "TP") !== false){
									//ligne CM
									$ligneTableau = $ligneTableau.'<tr><td>CM</td>';
									for($i = 1; $i < $maxSemaine + 1; $i++){
										$ligneTableau = $ligneTableau.'<td> </td>';
									}
									$ligneTableau = $ligneTableau.'</tr>';
									//ligne TD
									$ligneTableau = $ligneTableau.'<tr><td>TD</td>';
									for($i = 1; $i < $maxSemaine + 1; $i++){
										$ligneTableau = $ligneTableau.'<td> </td>';
									}
									$ligneTableau = $ligneTableau.'</tr>';
									//ligne TP
									$ligneTableau = $ligneTableau.'<tr><td>TP</td>';
									for($i = 1; $i < $maxSemaine + 1; $i++){
										if($i == $ligne['semaine']){
											$ligneTableau = $ligneTableau.'<td>'.$ligne['nbHeures'].'h</td>';
										}
										$ligneTableau = $ligneTableau.'<td> </td>';
									}
									$ligneTableau = $ligneTableau.'</tr>';
									$compteur1++;
								}
							
							}else{
								echo 'compteurElse : '.$compteurElse.'<br />';
								if($ligne['semaine'] != $doublonSemaine){
									$tab = explode("<td> </td>", $ligneTableau);
									
									if(strpos($ligne['partie'], "CM") !== false){
										$tab2 = array_splice($tab, $ligne['semaine']-(1+$compteurElse));
									}elseif(strpos($ligne['partie'], "TD") !== false){
										$tab2 = array_splice($tab, $maxSemaine+$ligne['semaine']-(1+$compteurElse));
									}elseif(strpos($ligne['partie'], "TP") !== false){
										$tab2 = array_splice($tab, 2*$maxSemaine+$ligne['semaine']-(1+$compteurElse));
									}
									$ligne1 = implode("<td> </td>", $tab);
								
									$tab2[0] = '<td>'.$ligne['nbHeures'].'h</td>';
									$ligne2 = implode("<td> </td>", $tab2);
								}else{
									$tab = explode("<td> </td>", $ligneTableau);
									if(strpos($ligne['partie'], "CM") !== false){
										$tab2 = array_splice($tab, $ligne['semaine']);
									}elseif(strpos($ligne['partie'], "TD") !== false){
										$tab2 = array_splice($tab, $maxSemaine+$ligne['semaine']);
									}elseif(strpos($ligne['partie'], "TP") !== false){
										$tab2 = array_splice($tab, 2*$maxSemaine+$ligne['semaine']);
									}
									$tailleTab = sizeof($tab);
									$nbCaractereAConserver = strlen($tab[$tailleTab-1]);
									$tab[$tailleTab-1] = str_replace("</td>", '+'.$ligne['nbHeures'].'h</td>', $tab[$tailleTab-1]);
									
									$ligne1 = implode("<td> </td>", $tab);
									$ligne2 = implode("<td> </td>", $tab2);
								}
								$compteurElse++;
								$ligneTableau = $ligne1.$ligne2;
							}
							$doublonSemaine = $ligne['semaine'];
							$doublonLigne = $ligne['module'];
						}
					
						$tableau[] = $ligneTableau;
					}
					$tableau[] = '</table>';
				}
				
				return $tableau;
			}
	
?>
<!DOCTYPE>
<html>
	<head>
		<title>TestAffectationSemaine.php</title>
		<meta charset="utf-8" />
	</head>
	<body>
		<h3>Test de recupPlanningParSemaineParPromotion($promotion)</h3>
		<?php
			$af = new AffectationSemaineManager($connection);
			$s = 4;
			$p = $promotions[1];
			$algoc1 = $af->recupPlanningParPromotion($p);
			echo '<pr>';
			print_r($algoc1);
			echo '</pr>';
		?>
		<h3>Affichage du planning par promotion</h3>
		<form action="AffectationSemaine.test.php" method="post">
			<select name="choix">
			<?php
			foreach($promotions as $promoElem){ 
				echo '<option value="'.$promoElem.'">'.$promoElem.'</option>';
			}
			?>
			</select>
			<input type="submit" name="Valider" />
		</form>
		<?php 	
			$planning = afficherTableau($connection);
			foreach($planning as $value){
				echo $value;
			}
		?>
	</body>
</html>
