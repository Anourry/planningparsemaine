<?php
    /**
    *   @author : Jean-Baptiste Louvet
    *   Classe ContenuModule qui modélise un contenumodule avec comme attributs :
    *       - id : l'id du module ("module" dans la BDD)
    *       - partie : la partie du module concerné
    *       - type : le type de contenu (TP, TD, CM, ...)
    *       - nbHeures : le nombre d'heures alloué à cette partie du module
    *       - enseignant : l'enseignant référent pour cette partie du module
    *   Primitives :
    *       - setters
    *       - getters
    **/
    
    require_once("Metier.class.php");
    
    class ContenuModule extends Metier{
        private $_module;
        private $_partie;
        private $_type;
        private $_nbHeures;
        private $_enseignant;
        
        
        
        /**
        ***************** SETTERS ****************
        **/
        public function setModule($module){
            $this->_module = $module;
        }
        
        public function setPartie($partie){
            $this->_partie = $partie;
        }
        
        public function set_Type($type){
            $this->_type = $type;
        }
        
        public function setNbHeures($nbHeures){
            $this->_nbHeures = $nbHeures;
        }
        
        public function setEnseignant($enseignant){
            $this->_enseignant = $enseignant;
        }
        
        
        
        
        /**
        ***************** GETTERS ****************
        **/
        public function getModule(){
            return $this->_module;
        }
        
        public function getPartie(){
            return $this->_partie;
        }
        
        public function get_Type(){
            return $this->_type;
        }
        
        public function getNbHeures(){
            return $this ->_nbHeures;
        }
        
        public function getEnseignant(){
            return $this->_enseignant;
        }
        
    }
?>
