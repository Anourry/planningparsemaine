<?php
    require_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");
    requireClass("Module");
?>
<!DOCTYPE>
<html>
	<head>
		<title>TestModule.php</title>
		<meta charset="utf-8" />
	</head>
	<body>
<?php	
	$module_array = array("id" => "ALGOC1", "public" => "TC", "semestre" => "s1", "libelle" => "Algorithmique et langage C1", "responsable" => "fgoasdoue");
	$module1 = new Module($module_array);
	$module1->affiche();
	
	$manager = new ModuleManager($connection);
	
	$module1 = $manager->recupById("DOO");
	$module1->affiche();
?>
	</body>
</html>
