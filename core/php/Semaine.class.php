<?php
	require_once("Metier.class.php");
	
	/**
	*	@author : Jean-Michel Nokaya
	*	Classe Semaine qui modélise une semaine avec comme attributs :
	*		- semaine : le numéro de la semaine
	*		- nombreHeuresMax : le nombre d'heures max
	*		- description : utilisé pour préciser s'il s'agit d'une semaine de vacances ou non
	*	Primitives :
	*		- setters
	*		- getteurs
	**/
	
	class Semaine extends Metier{
		private $_semaine;
		private $_nombreHeuresMax;
		private $_description;
		
		/**
		***************** SETTERS ****************
		**/
		public function setSemaine($semaine){
			$this->_semaine = $semaine;
		}
		
		public function setnombreHeuresMax($nbMaxh){
			$this->_nombreHeuresMax = $nbMaxh;
		}
		
		public function setDescription($desc){
			$this->_description = $desc;
		}
		
		
		/**
		***************** GETTERS ****************
		**/
		public function getSemaine(){
			return $this->_semaine;
		}
		
		public function getNombreHeuresMax(){
			return $this->_nombreHeuresMax;
		}
		
		public function getDescription(){
			return $this->_description;
		}
		
	}
?>
