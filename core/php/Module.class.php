<?php
	/**
	*	@author : Jean-Michel Nokaya et Jean-Baptiste Louvet
	*	Classe Module qui modélise un module avec comme attributs :
	*		- id : l'id du module ("module" dans la BDD)
	*		- public : le public auquel est destiné le module
	*		- semestre : le semestre pendant lequel le module se déroule
	*		- libelle : le nom complet du module
	*		- responsable : l'enseignant responsable du module
	*	Primitives :
	*		- setters
	*		- getteurs
	**/
	
    require_once("Metier.class.php");
	
	class Module extends Metier{
		private $_module;
		private $_public;
		private $_semestre;
		private $_libelle;
		private $_responsable;
		
		
		
		/**
		***************** SETTERS ****************
		**/
		public function setModule($module){
			$this->_module = $module;
		}
		
		public function setPublic($public){
			$this->_public = $public;
		}
		
		public function setSemestre($semestre){
			$this->_semestre = $semestre;
		}
		
		public function setLibelle($libelle){
			$this->_libelle = $libelle;
		}
		
		public function setResponsable($responsable){
			$this->_responsable = $responsable;
		}
		
		
		
		
		/**
		***************** GETTERS ****************
		**/
		public function getModule(){
			return $this->_module;
		}
		
		public function getPublic(){
			return $this->_public;
		}
		
		public function getSemestre(){
			return $this->_semestre;
		}
		
		public function getLibelle(){
			return $this ->_libelle;
		}
		
		public function getResponsable(){
			return $this->_responsable;
		}
		
	}
?>
