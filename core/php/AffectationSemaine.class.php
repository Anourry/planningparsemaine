<?php

    require_once("Metier.class.php");

	/**
	*	@author : Jean-Michel Nokaya
	*	Classe AffectationSemaine qui modélise une affectation d'un module et d'une semaine avec comme attributs :
	*		- module : l'id du module ("module" dans la BDD)
	*		- partie : nom de la partie du module (CM, TD, TP ...)
	*		- semaine : numéro de la semaine à laquelle le module est affecté
	*		- nbHeures : nomber d'heures du module dans la semaine donnée
	*		- commentaire : précision(s) apportée(s) à cette affectation module/semaine
	*	Primitives :
	*		- setters
	*		- getteurs
	**/
	class AffectationSemaine extends Metier{
		private $_module;
		private $_partie;
		private $_semaine;
		private $_nbHeures;
		private $_commentaire;
		
		
		/**
		***************** SETTERS ****************
		**/
		public function setModule($module){
			$this->_module = $module;
		}
		
		public function setPartie($partie){
			$this->_partie = $partie;
		}
		
		public function setSemaine($semaine){
			$this->_semaine = $semaine;
		}
		
		public function setNbHeures($nbHeures){
			$this->_nbHeures = $nbHeures;
		}
		
		public function setCommentaire($commentaire){
			$this->_commentaire = $commentaire;
		}
		
		
		
		/**
		***************** GETTERS ****************
		**/
		public function getModule(){
			return $this->_module;
		}
		
		public function getPartie(){
			return $this->_partie;
		}
		
		public function getSemaine(){
			return $this->_semaine;
		}
		
		public function getNbHeures(){
			return $this->_nbHeures;
		}
		
		public function getCommentaire(){
			return $this->_commentaire;
		}
		
	}
?>
