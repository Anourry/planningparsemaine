<?php
	/**
	*	@author : Jean-Michel Nokaya et Jean-Baptiste Louvet
	*	Classe Metier : classe mère dont hérite les classes métiers.
	**/	
	abstract class Metier{
        
        public function __construct(array $donnees = NULL){            
            if($donnees != null){
                $this->hydrate($donnees);
            }
        }
		
		/**
		*	Constructeur : utilise le principe de l'hydratation : récupération des données depuis
		*	la BDD sous forme de tableau de données grâce à la classe Metier_manager qui ajoute une 
		*	couche d'abstraction entre la BDD et le modèle métier.
		*/
		public function hydrate(array $donnees){
			// *********** 'hydrate' *********** //
			foreach($donnees as $key => $value){
				// ********** ucfirst : met le premier caractère en majuscule ********** //
				$method = 'set'.ucfirst($key);
				// ********** réalise les méthodes 'setters' pour donner une valeur aux attributs ********** //
				if ($method == "setType"){
                    			$method = "set_Type";
                		}
                
                		if(method_exists($this, $method)){
					$this->$method($value);
				}
			}
		}
		
		
		/**
		***************** Méthode d'affichage ****************
		**/
		public function affiche(){
			echo "<pre>";
			var_dump($this);
			echo "</pre>";
		}
		
	}
?>
