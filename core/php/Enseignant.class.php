<?php
    /**
    *   Auteur : Antoine Nourry et Jean-Michel Nokaya
    **/
	include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");
	require_once 'Metier.class.php';
	
	class Enseignant extends Metier {
		private $_login;
		private $_pwd;
		private $_nom;
		private $_prenom;
		private $_statut;
		private $_statutaire;
		private $_actif;
		private $_administrateur;
		private $_promotion;
		
		
		public function __construct(array $donnees = NULL){            
		    if($donnees != null){
		        $this->hydrate($donnees);
		    }
		    if($this->_administrateur != 0){
		    	$tableau = $this->rechercheDansCSV($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/core/php/database/enseignants.csv");
		    	if(isset($tableau['promotion'])){
                    $this->setPromotion($tableau['promotion']);
                }else{
                    $this->setPromotion('');
                }
		    }else{
		    	$this->setPromotion('');
		    }
		}
		
		public function getLogin(){
			return $this->_login;
		}
		
		public function getPwd(){
			return $this->_pwd;
		}
		
		public function getNom(){
			return $this->_nom;
		}
		
		public function getPrenom(){
			return $this->_prenom;
		}
		
		public function getStatut(){
			return $this->_statut;
		}
		
		public function getStatutaire(){
			return $this->_statutaire;
		}
		
		public function getActif(){
			return $this->_actif;
		}
		
		public function getAdministrateur(){
			return $this->_administrateur;
		}
		
		public function getPromotion(){
			return $this->_promotion;
		}
		
		public function setLogin($login){
			$this->_login = $login;
		}
		
		public function setPwd($pwd){
			$this->_pwd = $pwd;
		}
		
		public function setNom($nom){
			$this->_nom = $nom;
		}
		
		public function setPrenom($prenom){
			$this->_prenom = $prenom;
		}
		
		public function setStatut($statut){
			$this->_statut = $statut;
		}
		
		public function setStatutaire($statutaire){
			$this->_statutaire = $statutaire;
		}
		
		public function setActif($actif){
			$this->_actif = $actif;
		}
		
		public function setAdministrateur($administrateur){
			$this->_administrateur = $administrateur;
		}
		
		public function setPromotion($promotion){
			$this->_promotion = $promotion;
		}
		
		/*
		 * rechercheDansCSV : retourne un tableau dans lequel figurent les valeur de login,
		 * administrateur, et promotion, dans cet ordre.
		 */
		public function rechercheDansCSV($nomFichier, $lgthMot = 1000, $tab = ',') {
			$tableau = array();
			if(($handle = fopen($nomFichier, "r")) !== FALSE){
				// ***** Initialisation du tableau avec comme clés la première ligne du fichier csv : création du tableau des clés
				$keys = array();
				if(($data = fgetcsv($handle, $lgthMot, $tab)) !== FALSE){
					for($i = 0; $i < count($data); $i++){
						$keys[$i] = trim($data[$i]);
					}	
				}
				
				while(($data = fgetcsv($handle, $lgthMot, $tab)) !== FALSE){
					foreach($data as $value){
						trim($value);
					}
					if(in_array($this->_login, $data)){	
						$tableau = array_combine($keys, $data);
					}
				}
				fclose($handle) ;
			}
			
			return $tableau;
		}
		
	}
?>
