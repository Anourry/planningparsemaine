<?php
    
    /**
    *   @author : Jean-Baptiste Louvet
    *   Classe ContenuModuleManager : classe intéragissant avec la base de données. 
    *   Elle permet d'insérer, de mettre à jour et d'extraire une ligne de la table ContenuModule.
    **/
    
    require_once("Manager.class.php");
    
    class ContenuModuleManager extends Manager{
        
        public function insert(ContenuModule $contenumodule){
            $requete = $this->_db->prepare('INSERT INTO ContenuModule set module = :module, partie = :partie, type = :type, nbHeures = :nbHeures, enseignant = :enseignant');
            
            $requete->bindValue(':module', $contenumodule->getModule());
            $requete->bindValue(':partie', $contenumodule->getPartie());
            $requete->bindValue(':type', $contenumodule->get_Type());
            $requete->bindValue(':nbHeures', $contenumodule->getNbHeures());
            $requete->bindValue(':enseignant', $contenumodule->getEnseignant());
            
            $requete->execute();
        }
        
        public function update(ContenuModule $contenumodule){
            $requete = $this->_db->prepare('UPDATE ContenuModule set type = :type, nbHeures = :nbHeures, enseignant = :enseignant WHERE module = :module AND partie = :partie');
            
            $requete->bindValue(':module', $contenumodule->getModule());
            $requete->bindValue(':partie', $contenumodule->getPartie());
            $requete->bindValue(':type', $contenumodule->get_Type());
            $requete->bindValue(':nbHeures', $contenumodule->getNbHeures());
            $requete->bindValue(':enseignant', $contenumodule->getEnseignant());
            
//            echo "UPDATE ContenuModule set type = '".$contenumodule->get_Type()."', nbHeures = ".$contenumodule->getNbHeures().", enseignant = '".$contenumodule->getEnseignant()."' WHERE module = '".$contenumodule->getModule()."' AND partie = '".$contenumodule->getPartie()."';<br/>";
            
            $requete->execute();
        }
        
        public function recupById($id){
            $requete = $this->_db->prepare('SELECT * FROM ContenuModule WHERE module = :module');
            
            $requete->bindValue(':module', $id);
            
            $requete->execute();
            
            $result = $requete->fetch();
            
            return new ContenuModule($result);
        }
    }
?>
