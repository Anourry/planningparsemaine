<?php
	
	/**
	*	@author : Jean-Michel Nokaya et Jean-Baptiste Louvet
	*	Classe ModuleManager : classe intéragissant avec la base de données. 
	*	Elle permet d'insérer, de mettre à jour et d'extraire une ligne de la table Module.
	**/
	
	require_once("Manager.class.php");
	
	class ModuleManager extends Manager{
		
		public function insert(Module $module){
			$requete = $this->_db->prepare('INSERT INTO Module set module = :module, public = :public, semestre = :semestre, libelle = :libelle, responsable = :responsable');
			
			$requete->bindValue(':module', $module->getModule());
			$requete->bindValue(':public', $module->getPublic());
			$requete->bindValue(':semestre', $module->getSemestre());
			$requete->bindValue(':libelle', $module->getLibelle());
			$requete->bindValue(':responsable', $module->getResponsable());
			
			$requete->execute();
		}
		
		public function update(Module $module){
            $requete = $this->_db->prepare('UPDATE Module set public = :public, semestre = :semestre, libelle = :libelle, responsable = :responsable WHERE module = :module');
            
            $requete->bindValue(':module', $module->getModule());
            $requete->bindValue(':public', $module->getPublic());
            $requete->bindValue(':semestre', $module->getSemestre());
            $requete->bindValue(':libelle', $module->getLibelle());
            $requete->bindValue(':responsable', $module->getResponsable());
            
//			echo "UPDATE Module set public = '".$module->getPublic()."', semestre = '".$module->getSemestre()."', libelle = '".$module->getLibelle()."', responsable = '".$module->getResponsable()."' WHERE module = '".$module->getModule()."';<br/>";
            
            $requete->execute();
		}
		
		public function recupById($id){
            $module = new Module();
			$donnees = array();
			$requete = $this->_db->prepare('SELECT * FROM Module WHERE module = :id');
			
			$requete->bindValue(':id', $id);
			
			$requete->execute();
			
			$donnees = $requete->fetch();
			
			$module->hydrate($donnees);
			
			return $module;
        }
        
        public function getAllModules(){
            $modules;
            $contenus;
            $requete = $this->_db->prepare('SELECT Module.module, public, semestre, libelle, responsable, partie, type, nbHeures, enseignant FROM Module JOIN ContenuModule ON Module.module = ContenuModule.module ORDER BY public, semestre, module, partie, type');
            $requete->execute();
                        
            while($donnees = $requete->fetch()){
                $modules[] = new Module($donnees);
                $contenus[] = new ContenuModule($donnees);
            }
            
            $result[] = $modules;
            $result[] = $contenus;
            
            return $result;
        }
        
        public function getAllPublic(){
            $publics;
            $requete = $this->_db->prepare("SELECT DISTINCT public FROM Module");
            $requete->execute();
            
            while($donnees = $requete->fetch()){
                $publics[] = $donnees['public'];
            }
            
            return $publics;
        }
	}
?>
