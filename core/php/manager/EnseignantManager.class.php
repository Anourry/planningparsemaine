<?php
	
	include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");
	require_once 'Manager.class.php';
	
	/**
	*	@author : Jean-Michel Nokaya, Antoine Nourry et Jean-Baptiste Louvet
	*	Classe AffectationSeamineManager : classe intéragissant avec la base de données. 
	*	Elle permet d'insérer, de mettre à jour et d'extraire une ligne de la table AffectationSemaine.
	**/
	class EnseignantManager extends Manager{
		
		public function insert(Enseignant $enseignant){
			$requete = $this->_db->prepare('INSERT INTO Enseignant set login = :login, pwd = :pwd, nom = :nom, prenom = :prenom, statut = :statut, statutaire = :statutaire, actif = :actif, administrateur = :administrateur');
			
			$requete->bindValue(':login', $enseignant->getLogin());
			$requete->bindValue(':pwd', $enseignant->getPwd());
			$requete->bindValue(':nom', $enseignant->getNom());
			$requete->bindValue(':prenom', $enseignant->getPrenom());
			$requete->bindValue(':statut', $enseignant->getStatut());
			$requete->bindValue(':statutaire', $enseignant->getStatutaire());
			$requete->bindValue(':actif', $enseignant->getActif());
			$requete->bindValue(':administrateur', $enseignant->getAdministrateur());
			
			$requete->execute();
		}
		
		public function update($enseignant){
			$requete = $this->_db->prepare('UPDATE Enseignant set pwd = :pwd, nom = :nom, prenom = :prenom, statut = :statut, statutaire = :statutaire, actif = :actif, administrateur = :administrateur WHERE login = :login');
			
			$requete->bindValue(':pwd', $enseignant->getPwd());
			$requete->bindValue(':nom', $enseignant->getNom());
			$requete->bindValue(':prenom', $enseignant->getPrenom());
			$requete->bindValue(':statut', $enseignant->getStatut());
			$requete->bindValue(':statutaire', $enseignant->getStatutaire());
			$requete->bindValue(':actif', $enseignant->getActif());
			$requete->bindValue(':administrateur', $enseignant->getAdministrateur());
			$requete->bindValue(':login', $enseignant->getLogin());
			
			return $requete->execute();
		}
		
		public function updateSansPWD($enseignant){
			$requete = $this->_db->prepare('UPDATE Enseignant set nom = :nom, prenom = :prenom, statut = :statut, statutaire = :statutaire, actif = :actif, administrateur = :administrateur WHERE login = :login');
			
			$requete->bindValue(':nom', $enseignant->getNom());
			$requete->bindValue(':prenom', $enseignant->getPrenom());
			$requete->bindValue(':statut', $enseignant->getStatut());
			$requete->bindValue(':statutaire', $enseignant->getStatutaire());
			$requete->bindValue(':actif', $enseignant->getActif());
			$requete->bindValue(':administrateur', $enseignant->getAdministrateur());
			$requete->bindValue(':login', $enseignant->getLogin());
			
			return $requete->execute();
		}
		
		public function recup($login, $pwd = NULL){		//renvois True si le pseudo-pwd match, False sinon. Si pas de pwd en parametre, renvois l'enseignant
		    if($pwd == NULL){
		        $donnees = array();
			    $requete = $this->_db->prepare('SELECT login, nom, prenom, statut, statutaire, actif, administrateur FROM Enseignant WHERE login = :login');
			
			    $requete->bindValue(':login', $login);
			
			    $requete->execute();
			
			    $donnees = $requete->fetch();
			    $enseignant = new Enseignant($donnees);
			
			    return $enseignant;
		    } else {
			    $donnees = array();
			    $requete = $this->_db->prepare('SELECT COUNT(*) as nbCorrespondant FROM Enseignant WHERE login = :login AND pwd = :pwd');
			
			    $requete->bindValue(':login', $login);
			    $requete->bindValue(':pwd', $pwd);
			
			    $requete->execute();
			
			    $donnees = $requete->fetch();
			    			    
			    if( $donnees['nbCorrespondant'] == 0 ){
				    return false;
			    }else{
				    return true;
			    }
		    }
		}
		
		public function getServices(){
            $requete = $this->_db->prepare("SELECT nom, prenom, (SELECT COUNT(AffectationSemaine.nbHeures) FROM AffectationSemaine JOIN ContenuModule ON AffectationSemaine.module = ContenuModule.module WHERE ContenuModule.enseignant = Enseignant.login AND ContenuModule.type = 'CM') as CM, (SELECT COUNT(AffectationSemaine.nbHeures) FROM AffectationSemaine JOIN ContenuModule ON AffectationSemaine.module = ContenuModule.module WHERE ContenuModule.enseignant = Enseignant.login AND ContenuModule.type = 'TD') as TD, (SELECT COUNT(AffectationSemaine.nbHeures) FROM AffectationSemaine JOIN ContenuModule ON AffectationSemaine.module = ContenuModule.module WHERE ContenuModule.enseignant = Enseignant.login AND ContenuModule.type = 'TP') as TP, (SELECT COUNT(AffectationSemaine.nbHeures) FROM AffectationSemaine JOIN ContenuModule ON AffectationSemaine.module = ContenuModule.module WHERE ContenuModule.enseignant = Enseignant.login AND ContenuModule.type = 'Projet') as Projet FROM Enseignant");
            $requete->execute();
            
            $donnees = null;
            
            while($donnees[] = $requete->fetch());
            
            return $donnees;
		}
		
		
		public function isAdmin($login){        //Renvois "dde" si le compte liee au login est un dde, annee si liee a un respo d'annee, rien autrement
		        $donnees = array();
			    $requete = $this->_db->prepare('SELECT administrateur as admin FROM Enseignant WHERE login = :login');
			
			    $requete->bindValue(':login', $login);
			
			    $requete->execute();
			
			    $donnees = $requete->fetch();
			    			    
			    if( $donnees['admin'] == 2 ){
				    return "dde";
			    }elseif( $donnees['admin'] == 1 ){
				    return "annee";
			    }else{
			        return "rien";
			    }
		}
		
		public function recupAll(){
            $requete = $this->_db->prepare("SELECT * FROM Enseignant ORDER BY prenom, nom");
            $requete->execute();
            
            while($anEnseignant = $requete->fetch()){
                $donnees[] = new Enseignant($anEnseignant);
            }
            
            return $donnees;
		}
		
		public function getCharge($enseignant){
            $requete = $this->_db->prepare("SELECT SUM(AffectationSemaine.nbHeures)as charge, semaine FROM AffectationSemaine JOIN ContenuModule ON AffectationSemaine.module = ContenuModule.module AND AffectationSemaine.partie = ContenuModule.partie WHERE ContenuModule.enseignant = :enseignant GROUP BY semaine ORDER BY semaine");
            
            $requete->bindValue(':enseignant', $enseignant->getLogin());
            
            $requete->execute();
            
            while($donnees[] = $requete->fetch());
            
            return $donnees;
		}
		
}
