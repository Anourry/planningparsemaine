<?php
    /**
    *   Auteurs : Jean-Baptiste Louvet et Jean-Michel Nokaya
    **/
	abstract class Manager{
		protected $_db;
		protected $_variablesDb = array("module", "partie", "semaine", "nbHeures", "commentaire",
						"type", "enseignant", 
						"login", "pwd", "nom", "prenom", "statut", "statutaire", "actif", "administrateur",
						"semestre", "libelle", 
						"semaine", "nombreHeuresMax", "description");
		
		public function __construct($db){
			$this->setDb($db);
		}
		
		public function setDb($db){
			$this->_db = $db;
		}
		
		public function preparerDonnees($objet){
			$donnees = array();
			foreach($this->_variablesDb as $values){
				$method = 'get'.ucfirst($values);
				if(method_exist($objet, $method)){
					$donnees[$values] = $this->$method();
				}
			}
			
			return $donnees;
		}
	}
?>
