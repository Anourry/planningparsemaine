<?php
	
	include_once($_SERVER['DOCUMENT_ROOT']."/PlanningParSemaine/planningparsemaine/generalIncludes/var.php");
	require_once 'Manager.class.php';
	
	/**
	*	@author : Jean-Michel Nokaya
	*	Classe SemaineManager : classe intéragissant avec la base de données. 
	*	Elle permet d'insérer, de mettre à jour et d'extraire une ligne de la table Semaine.
	**/
	class SemaineManager extends Manager{
		
		public function insert($semaine){
			$requete = $this->_db->prepare('INSERT INTO Semaine set semaine = :semaine, nombreHeuresMax = :nombreHeuresMax, description = :description');
			
			$requete->bindValue(':semaine', $$semaine->getSemaine());
			$requete->bindValue(':nombreHeuresMax', $semaine->getNombreHeuresMax());
			$requete->bindValue(':description', $semaine->getDescription());
			
			$requete->execute();
		}
		
		public function update($semaine){
			$requete = $this->_db->prepare('UPDATE Semaine set semaine = :semaine, nombreHeuresMax = :nombreHeuresMax, description = :description');
			
			$requete->bindValue(':semaine', $$semaine->getSemaine());
			$requete->bindValue(':nombreHeuresMax', $semaine->getNombreHeuresMax());
			$requete->bindValue(':description', $semaine->getDescription());
			
			$requete->execute();
		}
		
		public function recup(){
			$semaines = array();
			
			$requete = $this->_db->prepare('SELECT * FROM Semaine');
			$requete->execute();

			while($resultatRequete[] = $requete->fetch(PDO::FETCH_ASSOC));
			
			unset($resultatRequete[count($resultatRequete) - 1]);

			for($i = 0 ; $i < count($resultatRequete); $i++){
				$semaines[] = new Semaine($resultatRequete[$i]);
			}
			
			return $semaines;
		}
		
}
