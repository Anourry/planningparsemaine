<?php
	require_once("Manager.class.php");
	
	/**
	*	@author : Jean-Michel Nokaya et Jean-Baptiste Louvet
	*	Classe AffectationSeamineManager : classe intéragissant avec la base de données. 
	*	Elle permet d'insérer, de mettre à jour et d'extraire une ligne de la table AffectationSemaine.
	**/
	class AffectationSemaineManager extends Manager {
		
		public function insert(AffectationSemaine $affectationSemaine){
			$requete = $this->_db->prepare('INSERT INTO AffectationSemaine (module, partie, semaine, nbHeures, commentaire) VALUES (:module, :partie, :semaine, :nbHeures, :commentaire)');
			
			$requete->bindValue(':module', $affectationSemaine->getModule());
			$requete->bindValue(':partie', $affectationSemaine->getPartie());
			$requete->bindValue(':semaine', $affectationSemaine->getSemaine());
			$requete->bindValue(':nbHeures', $affectationSemaine->getNbHeures());
			$requete->bindValue(':commentaire', $affectationSemaine->getCommentaire());
			
			$requete->execute();
		}
		
		public function update(AffectationSemaine $affectationSemaine){
			$requete = $this->_db->prepare('UPDATE AffectationSemaine set nbHeures = :nbHeures, commentaire = :commentaire WHERE module = :module AND partie = :partie AND semaine = :semaine');
			
			$requete->bindValue(':module', $affectationSemaine->getModule());
			$requete->bindValue(':partie', $affectationSemaine->getPartie());
			$requete->bindValue(':semaine', $affectationSemaine->getSemaine());
			$requete->bindValue(':nbHeures', $affectationSemaine->getNbHeures());
			$requete->bindValue(':commentaire', $affectationSemaine->getCommentaire());
			
			$requete->execute();
		}
		
		public function delete(AffectationSemaine $affectationSemaine){
			$requete = $this->_db->prepare('DELETE FROM AffectationSemaine WHERE module = :module AND partie = :partie AND semaine = :semaine');
			
			$requete->bindValue(':module', $affectationSemaine->getModule());
			$requete->bindValue(':partie', $affectationSemaine->getPartie());
			$requete->bindValue(':semaine', $affectationSemaine->getSemaine());
			
			$requete->execute();
		}
		
		public function recup($module){
			$donnees = array();
			$requete = $this->_db->prepare('SELECT * FROM AffectationSemaine WHERE module = :module');
			
			$requete->bindValue(':module', $module);
			
			$requete->execute();
			
			$donnees = $requete->fetch();
			$affectationSemaine = new AffectationSemaine($donnees);
			
			return $affectationSemaine;
		}
		
		/**
		*	recupPlanningParSemaineParPromotion : création et récupération d'une table de jointure
		*	contenant les données nécessaires à l'affichage d'un planning selon une promotion et une semaine données (paramètres d'entrée)
		**/
		public function recupPlanningParPromotion($promotion){
            $affectations = null;
            
            $sql = 'SELECT AffectationSemaine.module as mod1, partie, semaine, nbHeures, public, '; //Sélection des colonnes de la base
			$sql .= '(SELECT COUNT( DISTINCT partie ) FROM AffectationSemaine GROUP BY AffectationSemaine.module HAVING module = mod1) as nbParties FROM AffectationSemaine, Module '; //Sélection du nombre de parties par cours
			$sql .= 'WHERE AffectationSemaine.module = Module.module AND Module.public = :public ORDER BY mod1, partie, semaine ASC'; //Discrimination des lignes selon la promotion et ordonnancement
						
			$requete = $this->_db->prepare($sql); 
			
			$requete->bindValue(':public', $promotion);
			
			$requete->execute();
						
			while($resultatRequete[] = $requete->fetch(PDO::FETCH_ASSOC));
			
			unset($resultatRequete[count($resultatRequete) - 1]);
			
			for($i = 0 ; $i < count($resultatRequete); $i++){
				$resultatRequete[$i]['module'] = $resultatRequete[$i]['mod1'];
				unset($resultatRequete[$i]['mod1']); 
				$affectations[] = new AffectationSemaine($resultatRequete[$i]);
			}
			
			return $affectations;
		}
		
		public function maxElemRequete($resultatRequete, $element){
			$max = 0;
			$method = 'get'.ucfirst($element);
			foreach($resultatRequete as $value){
				if($value->$method() > $max){
				    $max = $value->$method();
				}
			}
			
			return $max;
		}
		
		public function commitParModule($module, $tableauAffectationSemaine){
			$sqlDelete = 'DELETE FROM AffectationSemaine WHERE module = :module';
			$requete = $this->_db->prepare($sqlDelete);
			$requete->bindValue(':module', $module);
			if($requete->execute()){
				foreach($tableauAffectationSemaine as $value){
					$this->insert($value);
				}
			
				return 1;
			}else{
				return 0;
			}
		}
		
		public function recupOne($module, $partie, $semaine){
            $donnees = array();
            $requete = $this->_db->prepare('SELECT * FROM AffectationSemaine WHERE module = :module AND partie = :partie AND semaine = :semaine');
            
            $requete->bindValue(':module', $module);
            $requete->bindValue(':partie', $partie);
            $requete->bindValue(':semaine', $semaine);
            
            $requete->execute();
            
            if($donnees = $requete->fetch()){
                $affectationSemaine = new AffectationSemaine($donnees);
            } else {
                $affectationSemaine = NULL;
            }
            return $affectationSemaine;
        }
        
        public function loadLastTrace($nomFichier, $lgthMot = 1000, $tab = ','){
        	$tableau = array();
		if(($handle = fopen($nomFichier, "r")) !== FALSE){
			// ***** Initialisation du tableau avec comme clés la première ligne du fichier csv : création du tableau des clés
			$keys = array();
			if(($data = fgetcsv($handle, $lgthMot, $tab)) !== FALSE){
				for($i = 0; $i < count($data); $i++){
					$keys[$i] = trim($data[$i]);
				}	
			}
			
			while(($data = fgetcsv($handle, $lgthMot, $tab)) !== FALSE){	
					$tableau = array_combine($keys, $data);
			}
			var_dump($tableau);
			fclose($handle) ;
		}
			
			return $tableau;
        }
        
        public function saveTrace($nomFichier, $lgthMot = 1000, $tab = ',',  $trace){
        	if($handle = fopen($nomFichier, 'c') != FALSE){
        		while(($data = fgetcsv($handle, $lgthMot, $tab)) !== FALSE){};
        		fputcsv($handle, $trace);
        	}
        	
        	fclose($fp);
        
	}
	
	}
?>
